package helper

import (
	"bytes"
	"encoding/json"

	"github.com/golang/protobuf/jsonpb"
	"google.golang.org/protobuf/runtime/protoiface"
)

func ParseToStruct(m protoiface.MessageV1, data interface{}) error {
	var jspbMarshal jsonpb.Marshaler

	jspbMarshal.OrigName = true
	jspbMarshal.EmitDefaults = true

	js, err := jspbMarshal.MarshalToString(m)
	if err != nil {
		return err
	}

	err = json.Unmarshal([]byte(js), data)
	return err
}

func ParseToProto(data interface{}, m protoiface.MessageV1) error {
	var jspbUnmarshal jsonpb.Unmarshaler
	js, err := json.Marshal(data)
	if err != nil {
		return err
	}

	err = jspbUnmarshal.Unmarshal(bytes.NewBuffer(js), m)

	return err
}

func ParseToJson(data interface{}, object interface{}) error {
	js, err := json.Marshal(data)
	if err != nil {
		return err
	}

	err = json.Unmarshal(js, object)
	return err
}
