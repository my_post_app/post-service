// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.15.8
// source: post_grud_service.proto

package post_grud_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// PostGrudServiceClient is the client API for PostGrudService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type PostGrudServiceClient interface {
	GetPosts(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetPostsResponse, error)
	GetPostByID(ctx context.Context, in *IdReq, opts ...grpc.CallOption) (*Post, error)
	UpdatePost(ctx context.Context, in *Post, opts ...grpc.CallOption) (*Response, error)
	DeletePost(ctx context.Context, in *IdReq, opts ...grpc.CallOption) (*Response, error)
}

type postGrudServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewPostGrudServiceClient(cc grpc.ClientConnInterface) PostGrudServiceClient {
	return &postGrudServiceClient{cc}
}

func (c *postGrudServiceClient) GetPosts(ctx context.Context, in *emptypb.Empty, opts ...grpc.CallOption) (*GetPostsResponse, error) {
	out := new(GetPostsResponse)
	err := c.cc.Invoke(ctx, "/post_grud_service.PostGrudService/GetPosts", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *postGrudServiceClient) GetPostByID(ctx context.Context, in *IdReq, opts ...grpc.CallOption) (*Post, error) {
	out := new(Post)
	err := c.cc.Invoke(ctx, "/post_grud_service.PostGrudService/GetPostByID", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *postGrudServiceClient) UpdatePost(ctx context.Context, in *Post, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/post_grud_service.PostGrudService/UpdatePost", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *postGrudServiceClient) DeletePost(ctx context.Context, in *IdReq, opts ...grpc.CallOption) (*Response, error) {
	out := new(Response)
	err := c.cc.Invoke(ctx, "/post_grud_service.PostGrudService/DeletePost", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// PostGrudServiceServer is the server API for PostGrudService service.
// All implementations must embed UnimplementedPostGrudServiceServer
// for forward compatibility
type PostGrudServiceServer interface {
	GetPosts(context.Context, *emptypb.Empty) (*GetPostsResponse, error)
	GetPostByID(context.Context, *IdReq) (*Post, error)
	UpdatePost(context.Context, *Post) (*Response, error)
	DeletePost(context.Context, *IdReq) (*Response, error)
	mustEmbedUnimplementedPostGrudServiceServer()
}

// UnimplementedPostGrudServiceServer must be embedded to have forward compatible implementations.
type UnimplementedPostGrudServiceServer struct {
}

func (UnimplementedPostGrudServiceServer) GetPosts(context.Context, *emptypb.Empty) (*GetPostsResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPosts not implemented")
}
func (UnimplementedPostGrudServiceServer) GetPostByID(context.Context, *IdReq) (*Post, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetPostByID not implemented")
}
func (UnimplementedPostGrudServiceServer) UpdatePost(context.Context, *Post) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdatePost not implemented")
}
func (UnimplementedPostGrudServiceServer) DeletePost(context.Context, *IdReq) (*Response, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeletePost not implemented")
}
func (UnimplementedPostGrudServiceServer) mustEmbedUnimplementedPostGrudServiceServer() {}

// UnsafePostGrudServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to PostGrudServiceServer will
// result in compilation errors.
type UnsafePostGrudServiceServer interface {
	mustEmbedUnimplementedPostGrudServiceServer()
}

func RegisterPostGrudServiceServer(s grpc.ServiceRegistrar, srv PostGrudServiceServer) {
	s.RegisterService(&PostGrudService_ServiceDesc, srv)
}

func _PostGrudService_GetPosts_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(emptypb.Empty)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PostGrudServiceServer).GetPosts(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/post_grud_service.PostGrudService/GetPosts",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PostGrudServiceServer).GetPosts(ctx, req.(*emptypb.Empty))
	}
	return interceptor(ctx, in, info, handler)
}

func _PostGrudService_GetPostByID_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PostGrudServiceServer).GetPostByID(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/post_grud_service.PostGrudService/GetPostByID",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PostGrudServiceServer).GetPostByID(ctx, req.(*IdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _PostGrudService_UpdatePost_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(Post)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PostGrudServiceServer).UpdatePost(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/post_grud_service.PostGrudService/UpdatePost",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PostGrudServiceServer).UpdatePost(ctx, req.(*Post))
	}
	return interceptor(ctx, in, info, handler)
}

func _PostGrudService_DeletePost_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(IdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(PostGrudServiceServer).DeletePost(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/post_grud_service.PostGrudService/DeletePost",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(PostGrudServiceServer).DeletePost(ctx, req.(*IdReq))
	}
	return interceptor(ctx, in, info, handler)
}

// PostGrudService_ServiceDesc is the grpc.ServiceDesc for PostGrudService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var PostGrudService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "post_grud_service.PostGrudService",
	HandlerType: (*PostGrudServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "GetPosts",
			Handler:    _PostGrudService_GetPosts_Handler,
		},
		{
			MethodName: "GetPostByID",
			Handler:    _PostGrudService_GetPostByID_Handler,
		},
		{
			MethodName: "UpdatePost",
			Handler:    _PostGrudService_UpdatePost_Handler,
		},
		{
			MethodName: "DeletePost",
			Handler:    _PostGrudService_DeletePost_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "post_grud_service.proto",
}
