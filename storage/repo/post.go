package repo

import (
	"context"
	pbu "post_service/genproto/post_service"
	entity "post_service/storage/entities"
)

type PostsI interface {
	CreatePosts(ctx context.Context, req *entity.Post) (res *pbu.CreatePostResponse, err error)
	// GetFavouriteInstruments(ctx context.Context, req *pbu.InvestorId) (res *pbu.FavouriteInstrumentList, err error)
	// AddInvestorFavouriteInstrument(ctx context.Context, req *pbu.AddFavouriteInstrumentRequest) (res *pbu.FavouriteInstrument, err error)
	// RemoveInvestorFavouriteInstrument(ctx context.Context, req *pbu.FavouriteInstrument) (res *pbu.FavouriteInstrument, err error)
	// IsInstrumentFavourite(ctx context.Context, req *pbu.FavouriteInstrument) (res *pbu.IsInstrumentFavouriteResponse, err error)
}
