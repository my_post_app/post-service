package repo

type PostgresStorageI interface {
	PostsStorage() PostsI
	CloseDB()
}
