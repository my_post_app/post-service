package postgres

import (
	"context"
	pbu "post_service/genproto/post_service"
	entity "post_service/storage/entities"
	"post_service/storage/repo"

	"github.com/jackc/pgx/v4"
	"github.com/jackc/pgx/v4/pgxpool"
)

type postRepo struct {
	db *pgxpool.Pool
}

func NewPost(db *pgxpool.Pool) repo.PostsI {
	return &postRepo{db}
}

func (i *postRepo) CreatePosts(ctx context.Context, req *entity.Post) (res *pbu.CreatePostResponse, err error) {
	stmt := `
	INSERT INTO posts(id, user_id, title, body)
	VALUES($1, $2, $3, $4)
	ON CONFLICT (id) DO NOTHING`
	res = &pbu.CreatePostResponse{}

	err = i.db.QueryRow(ctx, stmt, req.Id, req.UserID, req.Title, req.Body).Scan()
	if err == pgx.ErrNoRows {
		err = nil
	}
	res.Message = "created"
	return
}
