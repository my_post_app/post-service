package service

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"post_service/config"
	"post_service/genproto/post_service"
	"post_service/pkg/logger"
	"post_service/storage"
	entity "post_service/storage/entities"

	"github.com/golang/protobuf/ptypes/empty"
)

type postService struct {
	cfg  config.Config
	log  logger.LoggerI
	strg storage.StorageI
	post_service.UnimplementedPostServiceServer
}

func NewPostService(cfg config.Config, log logger.LoggerI, strg storage.StorageI) *postService {
	return &postService{
		cfg:  cfg,
		log:  log,
		strg: strg,
	}
}

func (s *postService) CreatePost(ctx context.Context, req *empty.Empty) (res *post_service.CreatePostResponse, err error) {
	s.log.Info("CreatePost calling ...")
	url := "https://gorest.co.in/public/v1/posts?page=%d"
	for i := 1; i <= 50; i++ {
		response, err := s.GetPostReqest(fmt.Sprintf(url, i))
		if err != nil {
			s.log.Error(fmt.Sprintf("Error occured while getting posts from external service: %s", err.Error()))
			return nil, err
		}
		for _, v := range response.Data {
			_, err := s.strg.PostgresStorage().PostsStorage().CreatePosts(ctx, &v)
			if err != nil {
				s.log.Error(fmt.Sprintf("Error occured while inserting posts: %s", err.Error()))
				return nil, err
			}
		}
	}
	return &post_service.CreatePostResponse{
		Message: "Successfully created",
	}, nil
}

func (s *postService) GetPostReqest(url string) (res *entity.MetaData, err error) {
	resp, err := http.Get(url)
	if err != nil {
		fmt.Println("Can nott unmarshal the byte array")
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	err = json.Unmarshal(body, &res)
	if err != nil {
		fmt.Println("Can nott unmarshal the byte array")
		return nil, err
	}
	return
}
