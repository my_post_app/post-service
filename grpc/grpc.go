package grpc

import (
	"post_service/config"
	"post_service/grpc/service"
	"post_service/pkg/logger"
	"post_service/storage"

	"post_service/genproto/post_service"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	post_service.RegisterPostServiceServer(grpcServer, service.NewPostService(cfg, log, strg))
	reflection.Register(grpcServer)
	return
}
